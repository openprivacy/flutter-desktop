# Docker containers for performing Flutter desktop builds

Dockerfiles @ [git.openprivacy.ca/openprivacy/flutter-desktop](https://git.openprivacy.ca/openprivacy/flutter-desktop)

These docker containers incorporate or are build on [cirrusci](https://hub.docker.com/u/cirrusci) ([cirruslabs](https://github.com/cirruslabs)).

- linux-fstable-3.22.2 is flutter 3.22.2, glibc , apt installing the needed utils including npm, cucumber-html-reporter (now based on debian bullseye)
- windows-sdk30-fstable-3.22.2

- linux-fstable-3.13.4 is flutter 3.19.3, glibc 2.31, apt installing the needed utils including npm, cucumber-html-reporter )
- windows-sdk30-fstable-3.19.3 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (3.13.4), and Google Root cert gtsr1 (for future proofing pub get)

- linux-fstable-3.13.4 is flutter 3.13.4, glibc 2.31, apt installing the needed utils including npm, cucumber-html-reporter
- windows-sdk30-fstable-3.13.4 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (3.13.4), and Google Root cert gtsr1 (for future proofing pub get)
- linux-fstable-3.10.2 is flutter 3.10.2, glibc 2.31, apt installing the needed utils including npm, cucumber-html-reporter
- windows-sdk30-fstable-3.10.2 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (3.10.2), and Google Root cert gtsr1 (for future proofing pub get)
- linux-fstable-3.7.1 is flutter 3.7.1, glibc 2.31, apt installing the needed utils including npm, cucumber-html-reporter
	- incorperated [cirrusci](https://hub.docker.com/u/cirrusci) Dockerfiles directly into linux Dockerfile to control base OS version (revert to ubuntu 20.04) to get older glibc (2.31) to support older distros
- linux-fstable-3.3.8 is cirrusci/flutter:3.3.9 + apt installing the needed utils (glibc 2.31) (including npm and cucumber-html-reporter)
	- manually rebuilt parent containers with ubuntu 20.04 for glibc 2.31 for greater compatibility
- windows-sdk30-fstable-3.3.8 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (3.3.8), and Google Root cert gtsr1 (for future proofing pub get)
- linux-fstable-3.3.8 is cirrusci/flutter:3.3.8 + apt installing the needed utils (glibc 2.31) (including npm and cucumber-html-reporter) 
- linux-fstable-3.0.1 is cirrusci/flutter:3.0.1 + apt installing the needed utils (including npm and cucumber-html-reporter)
- linux-fstable-2.8.1 is cirrusci/flutter:2.8.0 + apt installing the needed utils (including npm and cucumber-html-reporter)
- linux-fstable-2.8.0 is cirrusci/flutter:2.8.0 + apt installing the needed utils
- linux-fstable-2.5.3 is cirrusci/flutter:2.5.3 + apt installing the needed utils
- linux-fdev2.5-rc is cirrusci/flutter:2.5.0-6pre + apt installing the needed utils
- linux-fdev2.2-rc is cirrusci/flutter:2.2 rc + apt installing the needed utils
- linux-dev is cirrusci/flutter:2.2 rc + apt installing the needed utils
- windows-sdk30-fstable-3.7.1 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (3.7.1), and Google Root cert gtsr1 (for future proofing pub get)
- windows-sdk30-fstable-3.0.1 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (3.0.1), and Google Root cert gtsr1 (for future proofing pub get)
- windows-sdk30-fstable-2.8.1 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (2.8.1), and Google Root cert gtsr1 (for future proofing pub get)
- windows-sdk30-fstable2.5.3 is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter stable (2.5.3), and Google Root cert gtsr1 (for future proofing pub get)
- windows-dev-sdk30-fdev2.5rc is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter dev (2.5rc), and Google Root cert gtsr1 (for future proofing pub get)
- windows-dev-sdk30-fdev2.3rc is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, Flutter dev (2.3rc), and Google Root cert gtsr1 (for future proofing pub get)
- windows-dev-sdk30-fdev2.2rc is cirrusci/android-sdk:30-windowsservercore-2019 with Visual Studio 16, and Flutter dev (2.2rc)

These docker containers are both being used with Drone CI to build Flutter desktop apps for Linux and Windows. Simply run `flutter build` with the appropriate OS. These containers should work with any other Docker based automated build system such as Gitlab and others.

Example [.drone.yml](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/.drone.yml)

If you find these containers useful, please consider [donating to Open Privacy Research Society](https://openprivacy.ca/donate/) as it takes work to assemble and maintain our containers and we're a small org. :)
